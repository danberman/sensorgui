#ifndef GUIMAINWINDOW_H
#define GUIMAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class GUIMainWindow;
}

class GUIMainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit GUIMainWindow(QWidget *parent = 0);
    ~GUIMainWindow();
private slots:

    //***********INITIALIZATION*****************//
    void init();
    void initStateVariables();
    void enableAll(bool);
    void slotConnectDisconnectSerial();


    //***********COM PORT*****************//
    void initSerial();
    void slotUpdatePortInfo(QString);
    void slotReadSerial();
    void sendSerial(char,long);

    //***********FILE LOGGING*****************//
    bool createOutputFile();
    void slotEnableFileLogging();
    void writeFilesv(uint32_t);


    //***********STEPPER CONTROL*****************//
    void slotEnableStepper(int);
    void slotSetStepperState();

    void initStepperState();
    void updateStepperState();

    //***********SERVO CONTROL*****************//
    void slotEnableServo(int);
    void slotSetServoState();
    void servoButtonEnable(bool);
    void initServoState();
    void updateServoState();


    //***********DC MOTOR CONTROL*****************//
    //Slots
    void slotEnableDC(int);
    void slotGUIControlMode();
    void slotSensorControlMode();
    void slotPIDPositionMode();
    void slotPIDVelocityMode();

    void slotSetDCStateSensor(); //sets fields for Sensor-Mode DC Control
    void slotSetDCStateGUI(); //sets fields for GUI Control

    //Functions
    void initDCState();

    void updateDCState();
    void updateDCStateSensor(); //Updates fields for Sensor-Mode DC Control
    void updateDCStateGUI(); //Updates fields for GUI Control
    void updateDCStatePosition(); //Updates fields for Position PID
    void updateDCStateVelocity(); //Updates fields for Velocity PID


    void setDCStatePosition(); //sets fields for Position PID
    void setDCStateVelocity(); //sets fields for Velocity PID

    void enableDCControlGUI(bool);
    void enableDCControlSensor(bool);

    void enableDCPIDPosition(bool);
    void enableDCPIDVelocity(bool);

    void enableDCControlRadios(bool);
    void enablePIDControlRadios(bool);


private:
    Ui::GUIMainWindow *ui;

};


#endif // GUIMAINWINDOW_H
