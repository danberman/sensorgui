#-------------------------------------------------
#
# Project created by QtCreator 2015-10-12T11:34:15
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SensorGUI
TEMPLATE = app

QT += serialport

SOURCES += main.cpp\
    guimainwindow.cpp

HEADERS  += \
    guimainwindow.h \
    protocoldefinitions.h

FORMS    += \
    guimainwindow.ui
