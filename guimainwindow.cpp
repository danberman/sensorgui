#include "guimainwindow.h"
#include "protocoldefinitions.h"
#include "ui_guimainwindow.h"
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QDebug>
#include <QPalette>
#include <QValidator>
#include <QMessageBox>
#include <QFile>
#include <QFileDialog>


//DEFINES
#define SERIAL_BAUD QSerialPort::Baud19200
#define SERIAL_DATA_BITS QSerialPort::Data8
#define SERIAL_PARITY QSerialPort::NoParity
#define SERIAL_FLOW_CONTROL QSerialPort::NoFlowControl
#define SERIAL_STOP_BITS QSerialPort::OneStop
#define SERIAL_MAX_READ_LENGTH 1000

#define STEPPER_MIN_GAIN 0
#define STEPPER_MAX_GAIN 65535

#define SERVO_MIN_GAIN 0
#define SERVO_MAX_GAIN 65535

#define DC_MIN_GAIN 0
#define DC_MAX_GAIN 65535

#define DC_MIN_POSITION -65535
#define DC_MAX_POSITION 65535
#define DC_MIN_VELOCITY -65535
#define DC_MAX_VELOCITY 65535


//Palettes
QPalette disabledLineEditPalette;
QPalette enabledLineEditPalette;

//State Variables
struct stateVariables{
//Control Variables
    //Stepper
    uint16_t stepperGain;
    int32_t stepperCurrentPos;
    uint16_t stepperSensorVal;
    bool stepperEnabled;

    //Servo
    uint16_t servoGain;
    uint16_t servoCurrentPos;
    uint16_t servoSensorVal;
    bool servoEnabled;

    //DC Motor
    uint16_t dcGain;
    uint16_t dcSensorVal;
    int32_t dcCurrentPos;
    int32_t dcCurrentVelocity;
    int32_t dcTargetPos;
    int32_t dcTargetVelocity;
    int16_t dcPWM;
    bool dcSensorControlMode; //true = sensor control mode, false = GUI control mode
    bool dcPositionPIDMode; //true = position PID control, false = velocity PID control
    bool dcEnabled; //true = motor enabled, false = motor disabled

    bool buttonStatus;
} sv;

//File Logging
bool logging;
QString filename;

//Communications
QSerialPort *serial;
QList<QSerialPortInfo> COMPorts;
QByteArray serialData;


GUIMainWindow::GUIMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GUIMainWindow)
{
    ui->setupUi(this);
    setWindowTitle(tr("Team F Motor Interface"));
    serial = new QSerialPort(this);
    serialData = QByteArray();
    slotUpdatePortInfo(QString(""));

    //***********GLOBAL INITIALIZATION*****************//
    disabledLineEditPalette.setColor(QPalette::Base, QColor(190,190,190)); //Grey
    enabledLineEditPalette.setColor(QPalette::Base, QColor(255,255,255)); //White

    //***********GUI INITIALIZATION*****************//
    init();
    //***********FILE OUTPUT****************//
    connect(ui->LogFileButton,SIGNAL(clicked(bool)),this,SLOT(slotEnableFileLogging()));
    //***********COM PORT CONNECTS****************//
    connect(ui->COMComboBox,SIGNAL(activated(QString)),this,SLOT(slotUpdatePortInfo(QString)));
    connect(ui->COMConnectButton,SIGNAL(clicked(bool)),this,SLOT(slotConnectDisconnectSerial()));
    connect(serial,SIGNAL(readyRead()),this,SLOT(slotReadSerial()));
    //***********STEPPER CONNECTS****************//
    connect(ui->StepperEnableCheck,SIGNAL(stateChanged(int)),this,SLOT(slotEnableStepper(int)));
    connect(ui->StepperSetButton,SIGNAL(clicked(bool)),this,SLOT(slotSetStepperState()));
    //***********SERVO CONNECTS****************//
    connect(ui->ServoEnableCheck,SIGNAL(stateChanged(int)),this,SLOT(slotEnableServo(int)));
    connect(ui->ServoSetButton,SIGNAL(clicked(bool)),this,SLOT(slotSetServoState()));
    //***********DC MOTOR CONNECTS****************//
    //Enable
    connect(ui->DCEnableCheck,SIGNAL(stateChanged(int)),this,SLOT(slotEnableDC(int)));
    //Radio Buttons
    connect(ui->DCSensorControlRadio,SIGNAL(pressed()),this,SLOT(slotSensorControlMode()));
    connect(ui->DCGUIControlRadio,SIGNAL(pressed()),this,SLOT(slotGUIControlMode()));
    connect(ui->DCPositionControlRadio,SIGNAL(pressed()),this,SLOT(slotPIDPositionMode()));
    connect(ui->DCVelocityControlRadio,SIGNAL(pressed()),this,SLOT(slotPIDVelocityMode()));
    //Set Buttons
    connect(ui->DCSetGUIButton,SIGNAL(clicked(bool)),this,SLOT(slotSetDCStateGUI()));
    connect(ui->DCSetSensorButton,SIGNAL(clicked(bool)),this,SLOT(slotSetDCStateSensor()));
}


//***********INITIALIZATION*****************//
void GUIMainWindow::init()
{
    initStateVariables();
    initDCState();
    initServoState();
    initStepperState();
    initSerial();
    enableAll(false);
}

void GUIMainWindow::initStateVariables(){
    sv.stepperEnabled = false;
    sv.servoEnabled = false;
    sv.dcEnabled = false;
    sv.dcPositionPIDMode = true;    //Position PID
    sv.dcSensorControlMode = true; //Sensor Control Mode

    sv.servoEnabled = 0;
    sv.stepperEnabled = 0;
    sv.dcEnabled = 0;

    sv.servoGain = 5;
    sv.stepperGain = 5;
    sv.dcGain = 5;

    sv.dcGain = 10;
    sv.dcTargetPos = 0;
    sv.dcTargetVelocity = 0;
}

//***********COM PORT*****************//
void GUIMainWindow::slotUpdatePortInfo(QString text)
{
    int cmp = QString::compare(text,"Refresh",Qt::CaseInsensitive);
    if(cmp == 0){
        ui->COMComboBox->clear();
        QStringList list;
        COMPorts = QSerialPortInfo::availablePorts();
        Q_FOREACH(QSerialPortInfo port, COMPorts) {
                ui->COMComboBox->addItem(port.portName());
            }
        ui->COMComboBox->insertSeparator(ui->COMComboBox->maxCount() - 1);
        ui->COMComboBox->addItem(QString("Refresh"));
        ui->COMConnectButton->setDisabled(false); //enable connect button
    }
}
void GUIMainWindow::initSerial(){
    ui->COMConnectButton->setDisabled(true);
    ui->COMComboBox->addItem(QString("Refresh"));
    ui->COMComboBox->insertSeparator(0);
}
void GUIMainWindow::slotConnectDisconnectSerial(){
    if (serial->isOpen()) //If serial is open
    {
        serial->close();
        ui->COMConnectButton->setText("Connect");
        enableAll(false);
    }
    else
    {
        serial->setPort(COMPorts.value((ui->COMComboBox->currentIndex())));
        serial->setBaudRate(SERIAL_BAUD);
        serial->setDataBits(SERIAL_DATA_BITS);
        serial->setParity(SERIAL_PARITY);
        serial->setStopBits(SERIAL_STOP_BITS);
        serial->setFlowControl(SERIAL_FLOW_CONTROL);
        if (serial->open(QIODevice::ReadWrite)) {
            ui->COMConnectButton->setText("Disconnect");
            enableAll(true);
        } else {
            QMessageBox::critical(this, tr("Error"), serial->errorString());
        }
    }
}
void GUIMainWindow::sendSerial(char command, long value){
    if(serial->isOpen()){
        QByteArray data = QByteArray(QString(command).toLocal8Bit());
        QString valString = QString::number(value);
        data.append(valString.toLocal8Bit());
        data.append('\n');
        qDebug() << "Send: " << QString(data);
        serial->write(data);
    }
}

//***********FILE OUTPUT*****************//
bool GUIMainWindow::createOutputFile(){
    filename = QFileDialog::getSaveFileName(this,tr("Create Logging File"));
    filename.append(".csv");
    QFile f(filename);
    bool success = f.open(QIODevice::WriteOnly);
    if(success){
        logging = true;
        QTextStream fileStream(&f);
        fileStream << "Time" << ','
                           << "Target Position" << ','
                           << "Current Position" << ','
                           << "Target Velocity" << ','
                           << "Current Velocity" << ','
                           << "PWM Value" << ','
                           << "Stepper Gain" << ','
                           << "Stepper Pos" << ','
                           << "Pot" << ','
                           << "Stepper Enabled" << ','
                           << "Servo Gain" << ','
                           << "Servo Pos" << ','
                           << "Distance" << ','
                           << "Servo Enabled" << ','
                           << "DC Gain" << ','
                           << "Force Sensor" << ','
                           << "DC Sens Control Mode" << ','
                           << "DC Pos PID Mode" << ','
                           << "DC Enabled" << ','
                           << "Button Status" << '\n';


        f.close();
    }
    return success;
}

void GUIMainWindow::slotEnableFileLogging(){
    if(logging){ //Logging enabled, stop logging
        ui->LogFileButton->setText("Start Logging");
        logging = false;
    }
    else{ //Logging stopped, start logging
        createOutputFile();
        ui->LogFileButton->setText("Stop Logging");
    }
}

void GUIMainWindow::writeFilesv(uint32_t millis){
    QFile f(filename);
    bool success = f.open(QIODevice::WriteOnly | QIODevice::Append );
    if(success){
        QTextStream fileStream(&f);
        fileStream     << millis << ','
                       << sv.dcTargetPos << ','
                       << sv.dcCurrentPos << ','
                       << sv.dcTargetPos << ','
                       << sv.dcCurrentVelocity << ','
                       << sv.dcPWM << ','
                       << sv.stepperGain << ','
                       << sv.stepperCurrentPos << ','
                       << sv.stepperSensorVal << ','
                       << sv.stepperEnabled << ','
                       << sv.servoGain << ','
                       << sv.servoCurrentPos << ','
                       << sv.servoSensorVal << ','
                       << sv.servoEnabled << ','
                       << sv.dcGain << ','
                       << sv.dcSensorVal << ','
                       << sv.dcSensorControlMode << ','
                       << sv.dcPositionPIDMode << ','
                       << sv.dcEnabled << ','
                       << sv.buttonStatus << '\n';

        f.close();
    }
}

void GUIMainWindow::slotReadSerial(){
        char command, byte;
        int num;
        uint32_t millis;
        QString value;
        serialData+=serial->readAll();
        if(serialData.indexOf('\n') == -1) //if no newline
        {
            return;
        }
        if(serialData.length() > SERIAL_MAX_READ_LENGTH)
        {
            //qDebug() << "serialData " << serialData;
            serialData.clear();
            qDebug() << "Serial Read Buffer Cleared";
        }
  //      qDebug() << "d: " << QString(serialData);
        //Parse
        while(serialData.indexOf('\n') > -1)
        {
 //           qDebug() << "serialData " << serialData.data();
            command = serialData[0]; //get command
            serialData.remove(0,1);
            if(command < 'a' || command > 'p')
                continue;
            byte = serialData[0]; //get value
            bool failed = false;
            do{
                if(byte < '0' && byte > '9')
                {
                    failed = true;
                    break;
                }
                value+=(QString(byte));
                serialData.remove(0,1); //get next value
                byte = serialData[0];
            }while(byte != '\r' && serialData.length() > 0);
            if(failed)
                break;
            serialData.remove(0,1); //remove \r
  //          qDebug() << "Command: " << command << " Value: " << value << '\n';
            switch(command){
                case RESP_STEPPER_POSITION:
                    num = value.toLong();
                    //qDebug() << "Recd: " << QString(value);
                    sv.stepperCurrentPos = num;
                    ui->StepperCurrentPositionLineEdit->setText(QString::number(num));
                    break;
                case RESP_SERVO_POSITION:
                    num = value.toInt();
                    sv.servoCurrentPos = num;
                    ui->ServoCurrentPositionLineEdit->setText(QString::number(num));
                    break;
                case RESP_STEPPER_SENSOR:
                    num = value.toInt();
                    sv.stepperSensorVal = num;
                    ui->StepperSensorLineEdit->setText(QString::number(num));
                    break;
                case RESP_SERVO_SENSOR:
                    num = value.toInt();
                    sv.servoSensorVal = num;
                    ui->ServoSensorLineEdit->setText(QString::number(num));
                    break;
                case RESP_DC_SENSOR:
                    num = value.toInt();
                    sv.dcSensorVal = num;
                    ui->DCSensorLineEdit->setText(QString::number(num));
                    break;
                case RESP_BUTTON_STATUS:
                    num = value.toInt();
                    servoButtonEnable((bool)num);
                    break;
                case RESP_SERVO_GAIN:
                    //num = value.toInt();
                    //sv.servoGain = num;
                    break;
                case RESP_STEPPER_GAIN:
                    //num = value.toInt();
                    //sv.stepperGain = num;
                    //ui->StepperGainLineEdit->setText(QString::number(num));
                    break;
                case RESP_DC_POS:
                    num = value.toLong();
                    sv.dcCurrentPos = num;
                    ui->DCCurrentPositionGUILineEdit->setText(QString::number(num));
                    ui->DCCurrentPositionSensorLineEdit->setText(QString::number(num));
                    break;
                case RESP_DC_VEL:
                    num = value.toLong();
                    sv.dcCurrentVelocity = num;
                    ui->DCCurrentVelocityLineEdit->setText(QString::number(num));
                    break;
                case RESP_DC_GAIN:
                    //num = value.toInt();
                    //sv.dcGain = num;
                    //ui->DCGainLineEdit->setText(QString::number(num));
                    break;
                case RESP_DC_CONTROL_SENSOR:
                    num = value.toInt();
                    sv.dcSensorControlMode = num;
                    break;
                case RESP_DC_PID_POS:
                    num = value.toInt();
                    sv.dcPositionPIDMode = num;
                    break;
                case RESP_MILLIS:
                    //Update file only after all parameters have been received
                    millis = value.toLong();
                    if(logging)
                    {
                        writeFilesv(millis);
                    }
                    break;
                case RESP_DEBUG:
                    qDebug() << "Recd: " << value;
                    break;
                case RESP_PWM:
                    num = value.toInt();
                    sv.dcPWM = num;
                    qDebug() << "PWM = " << num;
                    break;
                default:
                    qDebug() << "Invalid Serial Response";
                        break;
            } //switch
            value.clear();
        }
}

//***********ENABLE CONTROLS*****************//
void GUIMainWindow::enableAll(bool enable){
    ui->StepperEnableCheck->setDisabled(not enable);
    ui->ServoEnableCheck->setDisabled(not enable);
    ui->DCEnableCheck->setDisabled(not enable);
    if(not enable){ //Disable all controllers
        slotEnableDC(false);
        slotEnableServo(false);
        slotEnableStepper(false);
    }
}

//***********STEPPER CONTROL*****************//
void GUIMainWindow::initStepperState(){
    //Line Edit Validators
    QValidator *stepperGainValidator = new QIntValidator(STEPPER_MIN_GAIN,STEPPER_MAX_GAIN,this);
    ui->StepperGainLineEdit->setValidator(stepperGainValidator);
    //Read Only line edits
    ui->StepperCurrentPositionLineEdit->setReadOnly(true);
    ui->StepperSensorLineEdit->setReadOnly(true);

    slotEnableStepper(false);
    updateStepperState();
}
void GUIMainWindow::updateStepperState(){
    QString gainStr = QString::number(sv.stepperGain);
    ui->StepperGainLineEdit->setText(gainStr);
}
void GUIMainWindow::slotSetStepperState(){
    QString gainStr = ui->StepperGainLineEdit->text();
    sv.stepperGain = gainStr.toInt();
    sendSerial(SET_STEPPER_GAIN,sv.stepperGain);
}
void GUIMainWindow::slotEnableStepper(int ienabled){
    QPalette palette;
    bool enabled = (bool)ienabled;
    sv.stepperEnabled = enabled;
    ui->StepperEnableCheck->setChecked(enabled);
    ui->StepperSensorLineEdit->setEnabled(enabled);
    ui->StepperGainLineEdit->setEnabled(enabled);
    ui->StepperCurrentPositionLineEdit->setEnabled(enabled);
    ui->StepperSetButton->setEnabled((int)enabled);

    if(enabled == 0)
        palette = disabledLineEditPalette;
    else
        palette = enabledLineEditPalette;

    ui->StepperSensorLineEdit->setPalette(palette);
    ui->StepperGainLineEdit->setPalette(palette);
    ui->StepperCurrentPositionLineEdit->setPalette(palette);

    updateStepperState();
    sendSerial(ENABLE_STEPPER,(int)sv.stepperEnabled);
}


//***********SERVO CONTROL*****************//
void GUIMainWindow::initServoState(){
    //Line Edit Validators
    QValidator *servoGainValidator = new QIntValidator(SERVO_MIN_GAIN,SERVO_MAX_GAIN,this);
    ui->ServoGainLineEdit->setValidator(servoGainValidator);
    //Read Only line edits
    ui->ServoCurrentPositionLineEdit->setReadOnly(true);
    ui->ServoSensorLineEdit->setReadOnly(true);
    ui->servoButtonEnable->setDisabled(true);
    slotEnableServo(0);
}
void GUIMainWindow::updateServoState(){
    QString gainStr = QString::number(sv.servoGain);
    ui->ServoGainLineEdit->setText(gainStr);
}
void GUIMainWindow::slotSetServoState(){
    QString gainStr = ui->ServoGainLineEdit->text();
    sv.servoGain = gainStr.toInt();
    sv.stepperGain = gainStr.toInt();
    sendSerial(SET_SERVO_GAIN,sv.servoGain);
}
void GUIMainWindow::servoButtonEnable(bool enable){
    ui->servoButtonEnable->setChecked(enable);
    sv.buttonStatus = enable;
    if(sv.servoEnabled && !ui->ServoEnableCheck->isChecked())
        slotEnableServo(true);
}
void GUIMainWindow::slotEnableServo(int ienabled)
{
    QPalette palette;
    ui->ServoEnableCheck->setChecked((bool)ienabled);
    sv.servoEnabled = (bool)ienabled;
    bool enabled = ((bool)ienabled & sv.buttonStatus);
    ui->ServoSensorLineEdit->setEnabled((bool)enabled);
    ui->ServoGainLineEdit->setEnabled((bool)enabled);
    ui->ServoCurrentPositionLineEdit->setEnabled((bool)enabled);
    ui->ServoSetButton->setEnabled(enabled);

    if(enabled == 0)
        palette =  disabledLineEditPalette;
    else
        palette = enabledLineEditPalette;

    ui->ServoSensorLineEdit->setPalette(palette);
    ui->ServoGainLineEdit->setPalette(palette);
    ui->ServoCurrentPositionLineEdit->setPalette(palette);

    updateServoState();
    sendSerial(ENABLE_SERVO,(int)sv.servoEnabled);
}


//***********DC MOTOR CONTROL*****************//
void GUIMainWindow::slotEnableDC(int ienabled)
{
    bool enabled = (bool)ienabled;
    sv.dcEnabled = enabled;
    ui->DCEnableCheck->setChecked(enabled);
    updateDCState();
    if(enabled){ //enable only selected section
        enableDCControlSensor(sv.dcSensorControlMode);
        enableDCControlGUI(not sv.dcSensorControlMode);
    }
   else{ //Disable all sections
        enableDCControlGUI(enabled);
        enableDCControlSensor(enabled);
    }
    enableDCControlRadios(enabled);
    sendSerial(ENABLE_DC,(int)sv.dcEnabled);
}

//Initialization
void GUIMainWindow::initDCState(){
    //Set up radio buttons
    ui->DCGUIControlRadio->setAutoExclusive(false);
    ui->DCSensorControlRadio->setAutoExclusive(false);
    ui->DCPositionControlRadio->setAutoExclusive(false);
    ui->DCVelocityControlRadio->setAutoExclusive(false);

    //Set lineEdit masks
    //ui->DCTargetPositionLineEdit->setMask(numericMasks32);
    //ui->DCTargetVelocityLineEdit->setMask(numericMaskss16);
    //ui->DCGainLineEdit->setMask(numericMasku16);

    QValidator *dcGainValidator = new QIntValidator(DC_MIN_GAIN,DC_MAX_GAIN,this);
    ui->DCGainLineEdit->setValidator(dcGainValidator);
    QValidator *dcTargetPosValidator = new QIntValidator(DC_MIN_POSITION,DC_MAX_POSITION,this);
    ui->DCTargetPositionLineEdit->setValidator(dcTargetPosValidator);
    QValidator *dcTargetVelValidator = new QIntValidator(DC_MIN_VELOCITY,DC_MAX_VELOCITY,this);
    ui->DCTargetVelocityLineEdit->setValidator(dcTargetVelValidator);

    //set read only line edits
    ui->DCCurrentPositionGUILineEdit->setReadOnly(true);
    ui->DCCurrentVelocityLineEdit->setReadOnly(true);
    ui->DCSensorLineEdit->setReadOnly(true);

    updateDCState();
    slotEnableDC(0);
}

//STATE VARIABLE UPDATES - update values shown in GUI from sv

//update all DC motor fields
void GUIMainWindow::updateDCState(){
    updateDCStateSensor();
    updateDCStateGUI();
}

void GUIMainWindow::updateDCStateSensor(){
    QString gainStr = QString::number(sv.dcGain);
    ui->DCGainLineEdit->setText(gainStr);
}
void GUIMainWindow::updateDCStateGUI(){
    updateDCStatePosition();
    updateDCStateVelocity();
}

void GUIMainWindow::updateDCStatePosition(){
    QString posStr = QString::number(sv.dcTargetPos);
    ui->DCTargetPositionLineEdit->setText(posStr);
}
void GUIMainWindow::updateDCStateVelocity(){
    QString velStr = QString::number(sv.dcTargetVelocity);
    ui->DCTargetVelocityLineEdit->setText(velStr);
}

void GUIMainWindow::slotSetDCStateSensor(){
    sv.dcSensorControlMode = true;
    QString gainStr = ui->DCGainLineEdit->text();
    sv.dcGain = gainStr.toInt();
    sendSerial(SET_DC_CONTROL_SENSOR,(int)sv.dcSensorControlMode);
    sendSerial(SET_DC_GAIN,sv.dcGain);
}
void GUIMainWindow::slotSetDCStateGUI(){
    QString str;
    sv.dcSensorControlMode = false;
    if(ui->DCPositionControlRadio->isChecked()){ //Position Control Mode
        setDCStatePosition();
        sv.dcPositionPIDMode = true;
        str = ui->DCTargetPositionLineEdit->text();
        sv.dcTargetPos = str.toInt();
        sendSerial(SET_DC_POS,sv.dcTargetPos);
    }
    else{
        setDCStateVelocity();
        sv.dcPositionPIDMode = false;
        str = ui->DCTargetVelocityLineEdit->text();
        sv.dcTargetVelocity = str.toInt();
        sendSerial(SET_DC_VEL,sv.dcTargetVelocity);
    }
    sendSerial(SET_DC_CONTROL_SENSOR,(int)sv.dcSensorControlMode);
    sendSerial(SET_DC_PID_POS,(int)sv.dcPositionPIDMode);
}


void GUIMainWindow::slotPIDPositionMode(){
    enablePIDControlRadios(false); //For some reason can only set radio states when buttons are disabled
    ui->DCPositionControlRadio->setChecked(true);
    ui->DCVelocityControlRadio->setChecked(false);
    enablePIDControlRadios(true);
    enableDCPIDPosition(true);
    enableDCPIDVelocity(false);
}

void GUIMainWindow::slotPIDVelocityMode(){
    enablePIDControlRadios(false); //For some reason can only set radio states when buttons are disabled
    ui->DCPositionControlRadio->setChecked(false);
    ui->DCVelocityControlRadio->setChecked(true);
    enablePIDControlRadios(true);
    enableDCPIDPosition(false);
    enableDCPIDVelocity(true);
}

void GUIMainWindow::setDCStatePosition(){
    QString posStr = ui->DCTargetPositionLineEdit->text();
    sv.dcTargetPos = posStr.toInt();
}
void GUIMainWindow::setDCStateVelocity(){
    QString velStr = ui->DCTargetVelocityLineEdit->text();
    sv.dcTargetVelocity = velStr.toInt();
}


//Widget Enables
//Enable widgets for GUI control mode
void GUIMainWindow::enableDCControlGUI(bool enabled){
    //GUI Control Visibility
    updateDCStateGUI();
    ui->DCSetGUIButton->setEnabled((bool)enabled);

    //Set Radio Buttons
    enableDCControlRadios(false); //For some reason can only set radio states when buttons are disabled
    ui->DCSensorControlRadio->setChecked(not enabled);
    ui->DCGUIControlRadio->setChecked(enabled);
    enableDCControlRadios(true);

    enablePIDControlRadios(false); //For some reason can only set radio states when buttons are disabled
    ui->DCPositionControlRadio->setChecked(sv.dcPositionPIDMode);
    ui->DCVelocityControlRadio->setChecked(not sv.dcPositionPIDMode);
    enablePIDControlRadios(enabled);

    if(enabled){
        //Enable only selected PID mode
        enableDCPIDPosition((int)sv.dcPositionPIDMode);
        enableDCPIDVelocity((int)not sv.dcPositionPIDMode);
    }
    else{
        //Disable Both
        enableDCPIDPosition(0);
        enableDCPIDVelocity(0);
    }
}
void GUIMainWindow::enableDCPIDPosition(bool enabled){
    QPalette palette;
    updateDCStatePosition();
    ui->DCCurrentPositionGUILineEdit->setEnabled((bool)enabled);
    ui->DCTargetPositionLineEdit->setEnabled((bool)enabled);
    if(enabled == 0)
        palette =  disabledLineEditPalette;
    else
        palette = enabledLineEditPalette;

    ui->DCCurrentPositionGUILineEdit->setPalette(palette);
    ui->DCTargetPositionLineEdit->setPalette(palette);
}
void GUIMainWindow::enableDCPIDVelocity(bool enabled){
    QPalette palette;
    updateDCStateVelocity();
    ui->DCCurrentVelocityLineEdit->setEnabled((bool)enabled);
    ui->DCTargetVelocityLineEdit->setEnabled((bool)enabled);
    if(enabled == 0)
        palette =  disabledLineEditPalette;
    else
        palette = enabledLineEditPalette;
    ui->DCCurrentVelocityLineEdit->setPalette(palette);
    ui->DCTargetVelocityLineEdit->setPalette(palette);
}
//Enable widgets for sensor control mode
void GUIMainWindow::enableDCControlSensor(bool enabled){
    QPalette palette;
    updateDCStateSensor();

    ui->DCSensorLineEdit->setEnabled(enabled);
    ui->DCGainLineEdit->setEnabled(enabled);
    ui->DCCurrentPositionSensorLineEdit->setEnabled(enabled);
    ui->DCSetSensorButton->setEnabled(enabled);


    if(enabled)
        palette =  enabledLineEditPalette;
    else
        palette = disabledLineEditPalette;

    ui->DCSensorLineEdit->setPalette(palette);
    ui->DCGainLineEdit->setPalette(palette);
    ui->DCCurrentPositionSensorLineEdit->setPalette(palette);

    //Set Radio Buttons
    enablePIDControlRadios(false);
    enableDCControlRadios(false); //For some reason can only set radio states when buttons are disabled
    ui->DCSensorControlRadio->setChecked(enabled);
    ui->DCGUIControlRadio->setChecked(not enabled);
    enableDCControlRadios(true);

}
//Enable control mode radio buttons
void GUIMainWindow::enableDCControlRadios(bool enabled){
    ui->DCSensorControlRadio->setEnabled(enabled);
    ui->DCGUIControlRadio->setEnabled(enabled);
}
void GUIMainWindow::enablePIDControlRadios(bool enabled){
    ui->DCPositionControlRadio->setEnabled(enabled);
    ui->DCVelocityControlRadio->setEnabled(enabled);
}

//Radio Buttons Callbacks

//Sets GUI Control Mode
void GUIMainWindow::slotGUIControlMode(){
    enableDCControlSensor(false);
    enableDCControlGUI(true);
}
//Sets Sensor Control Mode
void GUIMainWindow::slotSensorControlMode(){
    enableDCControlSensor(true);
    enableDCControlGUI(false);
}

GUIMainWindow::~GUIMainWindow()
{
    delete ui;
}
