#ifndef PROTOCOLDEFINITIONS
#define PROTOCOLDEFINITIONS

//Commands
#define SET_SERVO_GAIN 'A'
#define SET_STEPPER_GAIN 'B'
#define HOME_STEPPER 'C'
#define SET_DC_CONTROL_SENSOR 'D'
#define SET_DC_GAIN 'E'
#define SET_DC_PID_POS 'F'
#define SET_DC_VEL 'G'
#define SET_DC_POS 'H'
#define ENABLE_STEPPER 'I'
#define ENABLE_SERVO 'J'
#define ENABLE_DC 'K'
#define STOP_ALL 'X'

//Responses
#define RESP_STEPPER_POSITION 'a'
#define RESP_SERVO_POSITION 'b'
#define RESP_STEPPER_SENSOR 'c'
#define RESP_SERVO_SENSOR 'd'
#define RESP_DC_SENSOR 'e'
#define RESP_BUTTON_STATUS 'f'
#define RESP_SERVO_GAIN 'g'
#define RESP_STEPPER_GAIN 'h'
#define RESP_DC_POS 'i'
#define RESP_DC_VEL 'j'
#define RESP_DC_GAIN 'k'
#define RESP_DC_CONTROL_SENSOR 'l'
#define RESP_DC_PID_POS 'm'
#define RESP_MILLIS 'n'
#define RESP_DEBUG 'o'
#define RESP_PWM 'p'





//Responses

#endif // PROTOCOLDEFINITIONS

